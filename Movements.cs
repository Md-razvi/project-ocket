using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movements : MonoBehaviour
{
    Rigidbody rb;
    [SerializeField] float speedOfRocket=0;
    [SerializeField] float BodyMass=0.19f;
    
    // Start is called before the first frame update
    void Start()
    {
        rb=GetComponent<Rigidbody>();
          rb.mass=BodyMass;
    }

    // Update is called once per frame
    void Update()
    {
      
        ProcessThrust();
        ProcessRotation();
    }
    void ProcessThrust()
    {
        if(Input.GetKey(KeyCode.Space))
        {
            rb.AddRelativeForce(Vector3.up*Time.deltaTime*speedOfRocket);
        }
    }
    void ProcessRotation()
    {
         if(Input.GetKey(KeyCode.LeftArrow)||Input.GetKey(KeyCode.A))
        {
            RotationFrame(speedOfRocket);

        }
        else if(Input.GetKey(KeyCode.RightArrow)||Input.GetKey(KeyCode.D))
        {
            RotationFrame(-speedOfRocket);
        }
    }   
    void RotationFrame(float RotationThrust)
    {
        rb.freezeRotation=true;
        transform.Rotate(Vector3.forward * Time.deltaTime * RotationThrust);
        rb.freezeRotation=false;
    }
}
